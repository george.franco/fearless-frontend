window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference')

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
            const option = document.createElement('option')
            option.value = conference.href
            option.innerHTML = conference.name
            selectTag.appendChild(option)
        }
        //once conferences load we want to add or remove the d-none class that hides. Do this with .classList
        const hidden = document.getElementById('loading-conference-spinner');
        hidden.classList.add('d-none');
        selectTag.classList.remove('d-none');

        //Get the attendee form element by its id
        const formTag = document.getElementById('create-attendee-form'); 
        //Add an event handler for the submit event
        formTag.addEventListener('submit', async(event) => {
            event.preventDefault(); //Prevent the default from happening
            //Create a FormData object from the form
            const formData = new FormData(formTag)
            const json = JSON.stringify(Object.fromEntries(formData))

            const attendeeUrl = 'http://localhost:8001/api/attendees/'
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                }
            }
            const response = await fetch(attendeeUrl, fetchConfig)
            if (response.ok) {
                formTag.requestFullscreen();
                const newAttendee = await response.json()
                console.log(newAttendee)
            }
            formTag.classList.add('d-none');
            const success = document.getElementById('success-message');
            success.classList.remove('d-none')

        })
    }

})