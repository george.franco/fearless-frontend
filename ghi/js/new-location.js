window.addEventListener('DOMContentLoaded', async() => {
    const url = "http://localhost:8000/api/states/";

    try {
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json(); // gives list of states after formatting with .json()
            const selectTag = document.getElementById('state')
            for (let state of data.states) {
                //create option
                var option = document.createElement('option')

                //set .value of option to state's abbevriation
                option.value = state["abbreviation"]

                //set .innerHTML to states name
                option.innerHTML = state["name"]
                //append option as a child of select tag
                selectTag.appendChild(option)
            }

            const formTag = document.getElementById('create-location-form');
            formTag.addEventListener('submit', async(event) => {
                event.preventDefault(); //override default so browser doesnt handle post
                const formData = new FormData(formTag) //convert form data to json
                const json = JSON.stringify(Object.fromEntries(formData))

                const locationUrl = 'http://localhost:8000/api/locations/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
                const response = await fetch(locationUrl, fetchConfig)
                if (response.ok) {
                    formTag.reset()
                    const newLocation = await response.json()
                    console.log(newLocation)
                }
            })
        }
    } catch (e) {
        console.log("Oops, something went wrong");
    }
});

