window.addEventListener('DOMContentLoaded', () =>{
    const form = document.getElementById('login-form');
    form.addEventListener('submit', async(event) => {
        event.preventDefault()

        // const data = Object.fromEntries(new FormData(form))
        //comment out about and change body do to djwto requirements
        const fetchConfig = {
            method: 'post',
            body: new FormData(form),
        }
        const url = 'http://localhost:8000/login/';
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            window.location.href = '/';
        } else {
            console.error(response)
        }
    })
})