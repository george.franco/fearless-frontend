function createCard(name, startdate, enddate, description, pictureUrl, location) {
    return `
            <div class="card" style="box-shadow: 2px 2px; display: grid; grid-template-columns: 1fr 1fr 1fr;">
                <img src="${pictureUrl}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <p class="card-text">${location}</p>
                    <p class="card-text">${description}</p>
                </div><br>
                <div class="card-footer>
                    <p class="card-text" style="background-color: darkgrey; border-radius: 0 0 calc(0.25rem - 1px) calc(0.25rem - 1px); padding: 0.5rem 1rem;">${startdate} - ${enddate}</p>            
                </div>
            </div><br>
    `;
}
 
window.addEventListener('DOMContentLoaded', async() => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url)
   /* the fetch url and returns Promise fulfilled, only after adding async and await, otherwise will be pending */ 

        if (response.ok) {
            /* we add .json() to convert data to object that we can read */
            const data = await response.json();
            // change name to title i think
            for (let conference of data.conferences) {
                const detailURL = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailURL);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    console.log(details)
                    const name = details.conference.name;
                    const startdate = new Date(details.conference.starts).toLocaleDateString();
                    const enddate = new Date(details.conference.ends).toLocaleDateString();
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const location = details.conference.location.name
                    const html = createCard(name, startdate, enddate, description, pictureUrl, location);
                    const column = document.querySelector('.col-4');
                    column.innerHTML += html;
                }
            }
        }
    } catch (e) {
        console.log("error, something went wrong")
    }
});
