import React from 'react'

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            location: '',
            locations: [],
        }

        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleStartsChange = this.handleStartsChange.bind(this)
        this.handleEndChange = this.handleEndChange.bind(this)
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this)
        this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this)
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this)
        this.handleLocationChange = this.handleLocationChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    handleStartsChange(event) {
        const value = event.target.value;
        this.setState({starts: value})
    }

    handleEndChange(event) {
        const value = event.target.value;
        this.setState({ends: value})
    }

    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({description: value})
    }

    handleMaxPresentationsChange(event) {
        const value = event.target.value
        this.setState({max_presentations: value})
    }

    handleMaxAttendeesChange(event) {
        const value = event.target.value;
        this.setState({max_attendees: value})
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations
        console.log(data)
        

        const ConferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(ConferenceUrl, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();
          console.log(newConference);

        this.setState({
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            location: '',

        })
    }}
    

    async componentDidMount() {
        const url = "http://localhost:8000/api/locations/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations})
        }
    }

    render() {
        return (
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Create a new conference</h1>
                  <form id="create-conference-form">
                    <div className="form-floating mb-3">
                      <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" name="name" required type="text" id="name" className="form-control" />
                      <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={this.handleStartsChange} value={this.state.starts} placeholder="starts" name="starts" required type="date" id="starts" className="form-control" />
                      <label htmlFor="starts">Starts</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleEndChange} value={this.state.ends} placeholder="ends" name="ends" required type="date" id="ends" className="form-control" />
                        <label htmlFor="ends">Ends</label>
                      </div>
                    <div className="mb-3">
                        <label htmlFor="description" className="form-label">Description</label>
                        <textarea onChange={this.handleDescriptionChange} value={this.state.description} className="form-control" id="description" name="description" rows="3"></textarea>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleMaxPresentationsChange} value={this.state.max_presentations} placeholder="Maximum presentations" name="max_presentations" required type="number" id="max_presentations" className="form-control" />
                        <label htmlFor="max_presentations">Maximum presentations</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleMaxAttendeesChange} value={this.state.max_attendees} placeholder="Maximum attendees" name="max_attendees" required type="number" id="max_attendees" className="form-control" />
                        <label htmlFor="max_attendees">Maximum attendees</label>
                    </div>
                    <div className="mb-3">
                      <select onChange={this.handleLocationChange} value={this.state.location} required id="location" name="location" className="form-select">
                        <option value="">Choose a location</option>
                        {this.state.locations.map(locations => {
                            return (
                                <option value={locations.pk} key={locations.pk}>
                                    {locations.name}
                                </option>
                            )
                        })}
                      </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                  </form>
                </div>
              </div>
            </div>
        )
    }
}

export default ConferenceForm